//
//  UIViewControllerExt.swift
//  GoalPostApp
//
//  Created by Sumit Makkar on 11/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit

extension UIViewController
{
    func presentDetail(_ viewControllerToPresent: UIViewController)
    {
        let transition      = CATransition()
        transition.duration = 0.3
        transition.type     = CATransitionType.push
        transition.subtype  = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition , forKey: kCATransition)
        
        self.present(viewControllerToPresent , animated: false)
    }
    
    func presentSecondaryDetail(_ viewControllerToPresent: UIViewController)
    {
        let transition      = CATransition()
        transition.duration = 0.3
        transition.type     = CATransitionType.push
        transition.subtype  = CATransitionSubtype.fromRight
        guard let currentPresentedViewController = self.presentedViewController
        else
        {
            return
        }
        currentPresentedViewController.dismiss(animated: false)
        { [weak self] in
            self?.view.window?.layer.add(transition , forKey: kCATransition)
            self?.present(viewControllerToPresent , animated: false)
        }
    }
    
    func dismissDetail()
    {
        let transition      = CATransition()
        transition.duration = 0.3
        transition.type     = CATransitionType.push
        transition.subtype  = CATransitionSubtype.fromLeft
        self.view.window?.layer.add(transition , forKey: kCATransition)
        
        self.dismiss(animated: false)
    }
}
