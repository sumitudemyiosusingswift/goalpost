//
//  UIViewExt.swift
//  GoalPostApp
//
//  Created by Sumit Makkar on 11/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit


extension UIView
{
    func bindToKeyboard()
    {
        NotificationCenter.default.addObserver(self , selector: #selector(self.keyboardWillChange(_:)) , name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
        
    }
    
    @objc func keyboardWillChange(_ notification: NSNotification)
    {
        let duration      = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let curve         = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey]    as! UInt
        let startingFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey]       as! NSValue).cgRectValue
        let endingFrame   = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey]         as! NSValue).cgRectValue
        
        let deltaY        = endingFrame.origin.y - startingFrame.origin.y
        
        UIView.animateKeyframes(withDuration: duration , delay: 0.0 , options: UIView.KeyframeAnimationOptions(rawValue: curve) , animations:
            { [weak self] in
                DispatchQueue.main.async
                {
                    self?.frame.origin.y += deltaY
                }
            })
    }
}
