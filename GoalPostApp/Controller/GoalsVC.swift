//
//  ViewController.swift
//  GoalPostApp
//
//  Created by Sumit Makkar on 10/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as? AppDelegate

struct GoalInfoStruct
{
    var goalDescriptionString: String
    var goalCompletionValue: Int32
    var goalType: String
    var goalProgress: Int32
}

class GoalsVC: UIViewController
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var undoView: UIView!
    
    var goalsArray   = [Goal]()
    
    var deletedGoals = [GoalInfoStruct]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        
        self.tableView.isHidden   = false
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.fetchCoreData()
        
        self.tableView.reloadData()
    }
    
    func fetchCoreData()
    {
        self.fetchFromCoreData
                                { [weak self] (success) in
                                    if success
                                    {
                                        if goalsArray.count > 0
                                        {
                                            self?.tableView.isHidden = false
                                            
                                        }
                                        else
                                        {
                                            self?.tableView.isHidden = true
                                        }
                                    }
                                }
    }


    @IBAction func addGoalButtonAction(_ sender: Any)
    {
        guard let createGoalVC = storyboard?.instantiateViewController(withIdentifier: "createGoalVCIdentifier")
        else
        {
            return
        }
        
        self.presentDetail(createGoalVC)
    }
    
    @IBAction func undoButtonAction(_ sender: Any)
    {
        self.undoDeletedGoalsInCoreData()
        self.fetchCoreData()
        var allIndexPaths = [IndexPath]()
        var count         = self.goalsArray.count - self.deletedGoals.count
        while count < self.goalsArray.count
        {
            allIndexPaths.append(IndexPath(row: count , section: 0))
            count += 1
        }
//        self.tableView.reloadRows(at: allIndexPaths , with: UITableView.RowAnimation.automatic)
        self.tableView.reloadData()
        self.deletedGoals.removeAll()
    }
    
}



extension GoalsVC: UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.goalsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "goalCellIdentifier") as? GoalCell else
        {
            return UITableViewCell()
        }
        cell.configureCell(goal: self.goalsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle
    {
        return UITableViewCell.EditingStyle.none
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.destructive , title: "DELETE")
                            { [weak self] (rowAction , indexPath) in
                                self?.removeFromCoreData(AtIndexPath: indexPath)
                                self?.fetchCoreData()
                                tableView.deleteRows(at: [indexPath] , with: UITableView.RowAnimation.automatic)
                            }
        
        let addAction    = UITableViewRowAction(style: UITableViewRowAction.Style.normal , title: "Add 1")
                            { [weak self] (rowAction , indexPath) in
                                self?.setProgressGoal(AtIndexPath: indexPath)
                                tableView.reloadRows(at: [indexPath] , with: UITableView.RowAnimation.automatic)
                            }
        addAction.backgroundColor = #colorLiteral(red: 0.9176470588, green: 0.662745098, blue: 0.2666666667, alpha: 1)
        
        return [addAction , deleteAction]
    }
}


extension GoalsVC
{
    func setProgressGoal(AtIndexPath indexPath: IndexPath)
    {
        guard let managedContext = appDelegate?.persistentContainer.viewContext
            else
        {
            return
        }

        let chosenGoal = goalsArray[indexPath.row]
        if chosenGoal.goalProgress < chosenGoal.goalCompletionValue
        {
            chosenGoal.goalProgress += 1
        }
        else
        {
            return
        }
        
        do
        {
            try managedContext.save()
            print("Progress Saved in core data.")
        }
        catch
        {
            debugPrint("Could not save Progress : \(error.localizedDescription)")
        }
    }
    
    func fetchFromCoreData(completion: (_ complete: Bool) -> ())
    {
        guard let managedContext = appDelegate?.persistentContainer.viewContext
        else
        {
            return
        }
        let fetchRequest = NSFetchRequest<Goal>(entityName: "Goal")
        
        do
        {
            self.goalsArray = try managedContext.fetch(fetchRequest)
            print("Successfully fetched data.")
            completion(true)
        }
        catch
        {
            debugPrint("Could not fetch data : \(error.localizedDescription)")
            completion(false)
        }
    }
    
    func removeFromCoreData(AtIndexPath indexPath: IndexPath)
    {
        guard let managedContext = appDelegate?.persistentContainer.viewContext
        else
        {
            return
        }
        
        self.undoView.isHidden = false
        
        self.addNewDeleteGoal(deletedGoal: self.goalsArray[indexPath.row])

        managedContext.delete(self.goalsArray[indexPath.row])
        
        self.goalsArray.remove(at: indexPath.row)
        
        do
        {
            try managedContext.save()
            print("Successfullt removed goal.")
        }
        catch
        {
            debugPrint("Could not delete : \(error.localizedDescription)")
        }
    }
    
    func addNewDeleteGoal(deletedGoal: Goal)
    {
        let newDeletedGoal = GoalInfoStruct(goalDescriptionString: deletedGoal.goalDescription! , goalCompletionValue: deletedGoal.goalCompletionValue , goalType: deletedGoal.goalType! , goalProgress: deletedGoal.goalProgress)
        
        self.deletedGoals.append(newDeletedGoal)
    }
    
    func undoDeletedGoalsInCoreData()
    {
        for singleDeletedGoal in self.deletedGoals
        {
            self.saveInCoreData(deletedGoal: singleDeletedGoal)
        }
//        self.deletedGoals.removeAll()
    }
    
    func saveInCoreData(deletedGoal: GoalInfoStruct)
    {
        guard let managedContext = appDelegate?.persistentContainer.viewContext
            else
        {
            return
        }
        
        let goal                 = Goal(context: managedContext)
        goal.goalDescription     = deletedGoal.goalDescriptionString
        goal.goalType            = deletedGoal.goalType
        goal.goalCompletionValue = deletedGoal.goalCompletionValue
        goal.goalProgress        = deletedGoal.goalProgress
        
        do
        {
            try managedContext.save()
            print("Saved in core data.")
        }
        catch
        {
            debugPrint("Could not save : \(error.localizedDescription)")
        }
    }
}
