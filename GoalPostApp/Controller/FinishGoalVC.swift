//
//  FinishGoalVC.swift
//  GoalPostApp
//
//  Created by Sumit Makkar on 11/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit

class FinishGoalVC: UIViewController , UITextFieldDelegate
{
    @IBOutlet weak var createGoalButton: UIButton!
    @IBOutlet weak var pointsTextField: UITextField!
    
    var goalDescription: String!
    var goalType: GoalType!
    
    func initWithData(description: String , type: GoalType)
    {
        self.goalDescription = description
        self.goalType        = type
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.createGoalButton.bindToKeyboard()
        self.pointsTextField.delegate = self
    }
    
    @IBAction func createGoalAction(_ sender: Any)
    {
        self.saveInCoreData
                            { [weak self] (success) in
                                if success
                                {
                                    self?.dismissDetail()
                                    let presentedVC = self?.presentingViewController
                                    presentedVC?.dismiss(animated: false)
                                }
                            }
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        self.dismissDetail()
    }
    
    func saveInCoreData(_ completion: ((Bool) -> ()))
    {
        guard let managedContext = appDelegate?.persistentContainer.viewContext
        else
        {
            return
        }
        
        let goal                 = Goal(context: managedContext)
        goal.goalDescription     = self.goalDescription
        goal.goalType            = self.goalType.rawValue
        goal.goalCompletionValue = Int32(self.pointsTextField.text!)!
        goal.goalProgress        = Int32(0)
        
        do
        {
            try managedContext.save()
            print("Saved in core data.")
            completion(true)
        }
        catch
        {
            debugPrint("Could not save : \(error.localizedDescription)")
            completion(false)
        }
    }
}
