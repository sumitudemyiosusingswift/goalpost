//
//  CreateGoalVC.swift
//  GoalPostApp
//
//  Created by Sumit Makkar on 11/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit

class CreateGoalVC: UIViewController , UITextViewDelegate
{
    @IBOutlet weak var goalTextView: UITextView!
    @IBOutlet weak var shortTermButton: UIButton!
    @IBOutlet weak var longTermButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    internal var goalType: GoalType = .shortTerm
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.nextButton.bindToKeyboard()
        self.shortTermButton.setSelected()
        self.longTermButton.setDeselected()
        self.goalTextView.delegate = self
    }
    
    
    @IBAction func nextButtonAction(_ sender: Any)
    {
        if self.goalTextView.text != "" , self.goalTextView.text != "What is your goal?"
        {
            guard let finishVC = storyboard?.instantiateViewController(withIdentifier: "finishGoalVCIdentifier") as? FinishGoalVC else
            {
                return
            }
            finishVC.initWithData(description: self.goalTextView.text , type: self.goalType)

//            self.presentingViewController?.presentSecondaryDetail(finishVC)
            self.presentDetail(finishVC)
        }
    }
    
    @IBAction func shortTermButtonAction(_ sender: Any)
    {
        self.goalType = .shortTerm
        self.shortTermButton.setSelected()
        self.longTermButton.setDeselected()
    }
    
    @IBAction func longTermButtonAction(_ sender: Any)
    {
        self.goalType = .longTerm
        self.shortTermButton.setDeselected()
        self.longTermButton.setSelected()
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        self.dismissDetail()
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        self.goalTextView.text      = ""
        self.goalTextView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
}
