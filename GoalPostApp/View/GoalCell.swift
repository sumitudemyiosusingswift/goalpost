//
//  GoalCell.swift
//  GoalPostApp
//
//  Created by Sumit Makkar on 10/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit

class GoalCell: UITableViewCell
{
    @IBOutlet weak var goalDescriptionLabel: UILabel!
    @IBOutlet weak var goalTypeLabel: UILabel!
    @IBOutlet weak var goalProgressLabel: UILabel!
    @IBOutlet weak var goalCompletionView: UIView!
    
    func configureCell(goal: Goal)
    {
        self.goalDescriptionLabel.text = goal.goalDescription
        self.goalTypeLabel.text        = goal.goalType
        self.goalProgressLabel.text    = "\(goal.goalProgress)"
        
        if goal.goalProgress == goal.goalCompletionValue
        {
            self.goalCompletionView.isHidden = false
        }
        else
        {
            self.goalCompletionView.isHidden = true
        }
    }
    
}
