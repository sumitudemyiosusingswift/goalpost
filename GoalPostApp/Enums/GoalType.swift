//
//  GoalType.swift
//  GoalPostApp
//
//  Created by Sumit Makkar on 11/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import Foundation


enum GoalType: String
{
    case longTerm  = "Long Term"
    case shortTerm = "Short Term"
}
